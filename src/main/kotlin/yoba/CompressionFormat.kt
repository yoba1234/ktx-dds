package yoba

enum class CompressionFormat {
    DXT1,
    DXT3,
    DXT5;

    val bitsPerPixel: Int
        get() = if (this == DXT1) 8 else 16
}