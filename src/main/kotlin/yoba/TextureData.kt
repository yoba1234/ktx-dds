package yoba

data class TextureData(val width: Int, val height: Int, val mipmapCount: Int, val format: CompressionFormat, val images: List<ByteArray>)