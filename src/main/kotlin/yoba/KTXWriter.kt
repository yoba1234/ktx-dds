package yoba

import java.io.BufferedOutputStream
import java.io.DataOutputStream
import java.util.zip.GZIPOutputStream

private val headerMagic = byteArrayOf(0x0AB.toByte(), 0x04B.toByte(), 0x054.toByte(), 0x058.toByte(), 0x020.toByte(), 0x031.toByte(), 0x031.toByte(), 0x0BB.toByte(), 0x00D.toByte(), 0x00A.toByte(), 0x01A.toByte(), 0x00A.toByte())


fun writeKTX(output: BufferedOutputStream, t: TextureData) {

    val mipmapCount = t.mipmapCount
    var totalSize = headerMagic.size + 13 * 4
    for (level in 0..mipmapCount - 1) {
        totalSize += 4 + t.images[level].size
    }

    DataOutputStream(GZIPOutputStream(output)).use {
        it.run {
            writeInt(totalSize)
            write(headerMagic)
            writeInt(0x04030201)
            writeInt(0) // glType
            writeInt(1) // glTypeSize
            writeInt(0) // glFormat
            writeInt(getGlInternalFormat(t))
            writeInt(getGlBaseInternalFormat(t))
            writeInt(t.width)
            writeInt(t.height)
            writeInt(0) // depth (not supported)
            writeInt(0) // n array elements (not supported)
            writeInt(1) // nFaces (cubemaps are not supported yet)
            writeInt(mipmapCount)
            writeInt(0) // No additional info (key/value pairs)

            for (level in 0..mipmapCount - 1) {
                val image = t.images[level]
                val imageSize = image.size
                writeInt(imageSize)
                write(image)
            }
        }
    }
}

fun getGlBaseInternalFormat(t: TextureData): Int {
    return when (t.format) {
        CompressionFormat.DXT1 -> 0x1907
        else -> 0x1908
    }
}

fun getGlInternalFormat(t: TextureData): Int {
    return when (t.format) {
        CompressionFormat.DXT1 -> 0x83F0
        CompressionFormat.DXT3 -> 0x83F2
        CompressionFormat.DXT5 -> 0x83F3
    }
}
