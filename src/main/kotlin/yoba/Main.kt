package yoba

import com.google.common.io.LittleEndianDataInputStream
import java.io.BufferedInputStream
import java.io.BufferedOutputStream
import java.io.DataOutputStream
import java.nio.file.Files
import java.nio.file.Path
import java.nio.file.Paths

val exampleInputPath = Paths.get(System.getProperty("user.home"), "Desktop/yoba_transparent_im.dds")
val exampleOutputPath = Paths.get(System.getProperty("user.home"), "Desktop/yoba_transparent_im.zktx")

fun main(args: Array<String>) {
    convertDDStoKTX(exampleInputPath, exampleOutputPath)
}

fun convertDDStoKTX(exampleInputPath: Path, exampleOutputPath: Path) {
    val t = LittleEndianDataInputStream(BufferedInputStream(Files.newInputStream(exampleInputPath))).use(::readDDS)
    BufferedOutputStream(Files.newOutputStream(exampleOutputPath)).use { writeKTX(it, t) }
}
