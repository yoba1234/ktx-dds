package yoba

import com.google.common.io.LittleEndianDataInputStream
import yoba.CompressionFormat.DXT1
import yoba.CompressionFormat.DXT5
import java.io.BufferedInputStream
import java.io.DataInput
import java.lang.Math.max
import java.nio.file.Files
import java.nio.file.Paths
import kotlin.system.measureNanoTime

fun readDDS(i: DataInput): TextureData {
    return i.run {

        if (readInt() != 542327876) throw IllegalArgumentException() // magic number
        if (readInt() != 124) throw IllegalArgumentException() // size of header must be 124
        skipChecked(4)

        val height = readInt()
        val width = readInt()

        if (!height.isPowerOfTwo() || !width.isPowerOfTwo()) {
            throw IllegalArgumentException("Dimensions must be powers of two")
        }

        skipChecked(8)

        val mipmapCount = readInt()

        skipChecked((11 + 2) * 4) // unused area + first 2 DDS_PIXELFORMAT values

        val format = parseFormatFromFourcc(readInt())
        val bitsPerPixel = format.bitsPerPixel

        skipChecked((5 + 5) * 4)

        val images = (0..mipmapCount - 1).map {
            val levelWidth = max(1, width shr it)
            val levelHeight = max(1, height shr it)

            val byteSize = max(1, ((levelWidth + 3) / 4)) * max(1, ((levelHeight + 3) / 4)) * bitsPerPixel

            val content = ByteArray(byteSize)
            readFully(content)
            content
        }

        val remainingBytes = skipBytes(Int.MAX_VALUE)
        if (remainingBytes != 0) {
            throw IllegalArgumentException("Found $remainingBytes extra trailing bytes")
        }

        TextureData(width, height, mipmapCount, format, images)
    }
}

private fun Int.isPowerOfTwo(): Boolean {
    return this > 0 && (this and (this - 1)) == 0
}

fun parseFormatFromFourcc(bytes: Int): CompressionFormat {
    return when (bytes) {
        827611204 -> DXT1
        894720068 -> DXT5
        else -> TODO()
    }
}



private fun DataInput.skipChecked(byteCount: Int) {
    if (byteCount != skipBytes(byteCount)) throw IllegalStateException("Could not skip $byteCount bytes")
}
